def getCredentials(env, user):
    # Assuming that Project.Variables.Password contains the password.

    # Check for the environment and user to set the appropriate username.
    if env == "Val" and user == "Admin":
        username = "maniraj.kamaraj@venerable.com"
    elif env == "Dev" and user == "Admin":
        username = "maniraj.kamaraj@venerable.com"
    elif env == "Intg" and user == "Admin":
        username = "maniraj.kamaraj@venerable.com"
    else:
        # Handle other cases if required, or set a default username.
        # For now, let's assume a default username as an example.
        username = "default_username"

    password = Project.Variables.Password  # Assuming Project.Variables.Password contains the password.
    return username, password